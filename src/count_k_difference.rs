/// Given an integer array `nums` and an integer `k`, return the number of
/// pairs `(i, j)` where `i < j` such that `|nums[i] - nums[j]| == k`.
///
/// https://leetcode.com/problems/count-number-of-pairs-with-absolute-difference-k/
///
/// ## Constraints:
/// - `1 <= nums.length <= 200`
/// - `1 <= nums[i] <= 100`
/// - `1 <= k <= 99`
pub fn count_k_difference(nums: Vec<i32>, k: i32) -> i32 {
    let k = k as usize;

    let mut cnt = vec![0; 101];
    let mut res = 0;

    for n in nums.iter().map(|i| *i as usize) {
        cnt[n] += 1;
    }

    for i in (k + 1..101).map(|i| i as usize) {
        res += cnt[i] * cnt[i - k];
    }

    res
}

#[cfg(test)]
mod examples {
    use crate::count_k_difference::count_k_difference;

    #[test]
    fn example_1() {
        let nums = vec![1, 2, 2, 1];
        let k = 1;

        assert_eq!(4, count_k_difference(nums, k));
    }

    #[test]
    fn example_2() {
        let nums = vec![1, 3];
        let k = 3;

        assert_eq!(0, count_k_difference(nums, k));
    }

    #[test]
    fn example_3() {
        let nums = vec![3, 2, 1, 5, 4];
        let k = 2;

        assert_eq!(3, count_k_difference(nums, k));
    }

    #[test]
    fn example_4() {
        let nums = vec![1, 3, 3, 3, 5, 1, 1];
        let k = 2;

        assert_eq!(12, count_k_difference(nums, k));
    }
}
