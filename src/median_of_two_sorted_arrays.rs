/// Given two sorted arrays `nums1` and `nums2` of size `m` and `n` respectively,
/// return **the median** of the two sorted arrays.
///
/// The overall run time complexity should be `O(log (m+n))`.
///
/// https://leetcode.com/problems/median-of-two-sorted-arrays/
///
/// ## Constraints:
/// - `nums1.length == m`
/// - `nums2.length == n`
/// - `0 <= m <= 1000`
/// - `0 <= n <= 1000`
/// - `1 <= m + n <= 2000`
/// - `-10^6 <= nums1[i]`, `nums2[i] <= 10^6`
pub fn find_median_sorted_arrays(nums1: Vec<i32>, nums2: Vec<i32>) -> f64 {
    // this is O(n+m) though
    let merged = {
        let merged_len = nums1.len() + nums2.len();
        let mut result = vec![0; merged_len];
        let mut i = 0;
        let mut j = 0;
        let mut k = 0;

        loop {
            if k >= merged_len {
                break;
            }

            if i >= nums1.len() {
                result[k] = nums2[j];

                j += 1;
                k += 1;
                continue;
            }
            if j >= nums2.len() {
                result[k] = nums1[i];

                i += 1;
                k += 1;
                continue;
            }

            if nums1[i] > nums2[j] {
                result[k] = nums2[j];
                j += 1;
            } else {
                result[k] = nums1[i];
                i += 1;
            };

            k += 1;
        }

        result
    };

    let mid_idx = merged.len() / 2;
    if merged.len() & 1 == 1 {
        merged[mid_idx] as f64
    } else {
        let left = merged[mid_idx - 1];
        let right = merged[mid_idx];

        ((left + right) as f64) / 2.0
    }
}

#[cfg(test)]
mod median_of_two_sorted_arrays_examples {
    use crate::median_of_two_sorted_arrays::find_median_sorted_arrays;

    #[test]
    fn example_1() {
        let nums1 = vec![1, 3];
        let nums2 = vec![2];

        assert_eq!(2.0, find_median_sorted_arrays(nums1, nums2));
    }

    #[test]
    fn example_2() {
        let nums1 = vec![1, 2];
        let nums2 = vec![3, 4];

        assert_eq!(2.5, find_median_sorted_arrays(nums1, nums2));
    }

    #[test]
    fn example_3() {
        let nums1 = vec![0, 0];
        let nums2 = vec![0, 0];

        assert_eq!(0.0, find_median_sorted_arrays(nums1, nums2));
    }

    #[test]
    fn example_4() {
        let nums1 = vec![];
        let nums2 = vec![1];

        assert_eq!(1.0, find_median_sorted_arrays(nums1, nums2));
    }

    #[test]
    fn example_5() {
        let nums1 = vec![2];
        let nums2 = vec![];

        assert_eq!(2.0, find_median_sorted_arrays(nums1, nums2));
    }
}
