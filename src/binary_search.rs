use std::cmp::Ordering;

/// Given an array of integers nums which is sorted in ascending order,
/// and an integer target, write a function to search target in nums.
/// If target exists, then return its index. Otherwise, return -1.
///
/// You must write an algorithm with O(log n) runtime complexity.
///
/// https://leetcode.com/problems/binary-search/
///
/// ### Constraints:
///
/// - `1 <= nums.length <= 10^4`
/// - `-10^4 < nums[i], target < 10^4`
/// - All the integers in `nums` are **unique**.
/// - `nums` is sorted in ascending order.
pub fn search(nums: Vec<i32>, target: i32) -> i32 {
    match cmp(&nums, &target, 0, nums.len() - 1) {
        Ok(value) => value as i32,
        Err(_) => -1,
    }
}

pub fn search2(nums: Vec<i32>, target: i32) -> i32 {
    let mut lo = 0;
    let mut hi = nums.len() - 1;
    while lo < hi {
        let mid = lo + ((hi - lo + 1) / 2);
        if nums[mid] > target {
            hi = mid - 1;
        } else {
            lo = mid;
        }
    }

    if nums[lo] == target {
        lo as i32
    } else {
        -1
    }
}

fn cmp(nums: &Vec<i32>, target: &i32, left: usize, right: usize) -> Result<usize, String> {
    println!("l = {:?} r = {:?}", left, right);
    if right - left + 1 < 3 {
        return if nums[left] == *target {
            Ok(left)
        } else if nums[right] == *target {
            Ok(right)
        } else {
            Err(String::from("not found"))
        };
    }

    let mid = left + ((right - left + 1) / 2);
    match nums[mid].cmp(&target) {
        Ordering::Equal => Ok(mid),
        Ordering::Less => cmp(nums, target, mid + 1, right),
        Ordering::Greater => cmp(nums, target, left, mid - 1),
    }
}

#[cfg(test)]
mod examples {
    use crate::binary_search::search;

    #[test]
    fn example_1() {
        let nums = vec![-1, 0, 3, 5, 9, 12];
        let target = 9;

        assert_eq!(4, search(nums, target));
    }

    #[test]
    fn example_2() {
        let nums = vec![-1, 0, 3, 5, 9, 12];
        let target = 2;

        assert_eq!(-1, search(nums, target));
    }

    #[test]
    fn example_3() {
        let nums = vec![2, 5];
        let target = 0;

        assert_eq!(-1, search(nums, target));
    }

    #[test]
    fn example_4() {
        let nums = vec![2, 5];
        let target = 5;

        assert_eq!(1, search(nums, target));
    }

    #[test]
    fn example_5() {
        let nums = vec![2];
        let target = 2;

        assert_eq!(0, search(nums, target));
    }

    #[test]
    fn example_6() {
        let nums = vec![2];
        let target = 3;

        assert_eq!(-1, search(nums, target));
    }

    #[test]
    fn example_7() {
        let nums = vec![2];
        let target = 1;

        assert_eq!(-1, search(nums, target));
    }

    #[test]
    fn example_8() {
        let nums = vec![-1, 0, 5];
        let target = 0;

        assert_eq!(1, search(nums, target));
    }
}
