// Definition for singly-linked list.
#[derive(PartialEq, Eq, Clone, Debug)]
pub struct ListNode {
    pub val: i32,
    pub next: Option<Box<ListNode>>,
}

impl ListNode {
    #[inline]
    fn new(val: i32) -> Self {
        ListNode { next: None, val }
    }
}

fn make_linked_list(num: i32) -> Option<Box<ListNode>> {
    let num_string = num.to_string();
    let num_iter = num_string
        .split("")
        .into_iter()
        .filter(|x| !x.is_empty())
        .map(|x| x.parse::<i32>().unwrap())
        .map(|x| ListNode::new(x));

    let mut last_node = None;
    for mut node in num_iter {
        node.next = last_node;
        last_node = Some(Box::new(node));
    }

    last_node
}

/// You are given two non-empty linked lists representing two non-negative integers.
/// The digits are stored in reverse order, and each of their nodes contains a single digit.
/// Add the two numbers and return the sum as a linked list.
///
/// You may assume the two numbers do not contain any leading zero, except the number 0 itself.
///
/// https://leetcode.com/problems/add-two-numbers/submissions/
///
/// ### Constraints:
/// - The number of nodes in each linked list is in the range [1, 100].
/// - `0 <= Node.val <= 9`
/// - It is guaranteed that the list represents a number that does not have leading zeros.
pub fn add_two_numbers1(
    l1: Option<Box<ListNode>>,
    l2: Option<Box<ListNode>>,
) -> Option<Box<ListNode>> {
    if l1.is_none() && l2.is_none() {
        return None;
    }

    let mut result = Some(Box::new(ListNode::new(0)));
    let mut carry = 0;

    let mut l1 = l1.clone();
    let mut l1 = l1.as_deref_mut().unwrap();

    let mut l2 = l2.clone();
    let mut l2 = l2.as_deref_mut().unwrap();

    let mut node = result.as_deref_mut().unwrap();
    loop {
        let sum = l1.val + l2.val + carry;
        node.val = sum % 10;
        node.next = Some(Box::new(ListNode::new(0)));

        carry = sum / 10;

        if l1.next.is_none() && l2.next.is_none() {
            if carry > 0 {
                node.next = Some(Box::new(ListNode::new(carry)));
            } else {
                node.next = None;
            }
            break;
        }

        node = node.next.as_deref_mut().unwrap();

        if l1.next.is_none() {
            let n = Box::new(ListNode::new(0));
            l1.next = Some(n);
        }
        if l2.next.is_none() {
            let n = Box::new(ListNode::new(0));
            l2.next = Some(n);
        }

        l1 = l1.next.as_deref_mut().unwrap();
        l2 = l2.next.as_deref_mut().unwrap();
    }

    result
}

/// leetcode doesn't accept this solution due to their lower rustc version
pub fn add_two_numbers2(
    l1: Option<Box<ListNode>>,
    l2: Option<Box<ListNode>>,
) -> Option<Box<ListNode>> {
    if l1.is_none() || l2.is_none() {
        return None;
    }

    let num1 = linked_list_to_num(l1).unwrap();
    let num2 = linked_list_to_num(l2).unwrap();
    make_linked_list(num1 + num2)
}

fn linked_list_to_num(l: Option<Box<ListNode>>) -> Option<i32> {
    let mut num: Vec<i32> = vec![];

    let mut l = l.as_deref().unwrap();
    loop {
        num.push(l.val);

        if l.next.is_none() {
            break;
        }

        l = l.next.as_deref().unwrap();
    }

    num.iter()
        .enumerate()
        .map(|t| t.1 * i32::pow(10, t.0 as u32))
        .reduce(|a, b| a + b)
}

#[cfg(test)]
mod examples {
    use crate::add_two_numbers::add_two_numbers1;
    use crate::add_two_numbers::add_two_numbers2;
    use crate::add_two_numbers::make_linked_list;

    #[test]
    fn add_two_numbers_example_1() {
        let l1 = make_linked_list(342);
        let l2 = make_linked_list(465);
        assert_eq!(make_linked_list(807), add_two_numbers1(l1, l2));

        let l1 = make_linked_list(342);
        let l2 = make_linked_list(465);
        assert_eq!(make_linked_list(807), add_two_numbers2(l1, l2));
    }

    #[test]
    fn add_two_numbers_example_2() {
        let l1 = make_linked_list(0);
        let l2 = make_linked_list(0);
        assert_eq!(make_linked_list(0), add_two_numbers1(l1, l2));

        let l1 = make_linked_list(0);
        let l2 = make_linked_list(0);
        assert_eq!(make_linked_list(0), add_two_numbers2(l1, l2));
    }

    #[test]
    fn add_two_numbers_example_3() {
        let l1 = make_linked_list(9999);
        let l2 = make_linked_list(9999999);
        assert_eq!(make_linked_list(9999 + 9999999), add_two_numbers1(l1, l2));

        let l1 = make_linked_list(9999);
        let l2 = make_linked_list(9999999);
        assert_eq!(make_linked_list(9999 + 9999999), add_two_numbers2(l1, l2));
    }

    // l1  =  387 = 7 -> 8 -> 3
    // l2  = 3498 = 8 -> 9 -> 4 -> 3
    // carry =           1    1
    // -----------------------------
    // sum = 3885 = 5 -> 8 -> 8 -> 3

    #[test]
    fn add_two_numbers_example_4() {
        let l1 = make_linked_list(999999);
        let l2 = make_linked_list(9999);

        // 9 -> 9 -> 9 -> 9 -> 9 -> 9
        // 9 -> 9 -> 9 -> 9
        // 1    1    1    1    1    1    1
        // --------------------------
        // 8 -> 9 -> 9 -> 9 -> 0 -> 0 -> 1

        assert_eq!(make_linked_list(999999 + 9999), add_two_numbers1(l1, l2));

        let l1 = make_linked_list(999999);
        let l2 = make_linked_list(9999);
        assert_eq!(make_linked_list(999999 + 9999), add_two_numbers2(l1, l2));
    }

    #[test]
    fn add_two_numbers_example_5() {
        assert_eq!(None, add_two_numbers1(None, None));
        assert_eq!(None, add_two_numbers2(None, None));
    }
}
