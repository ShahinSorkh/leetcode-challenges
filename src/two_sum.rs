use std::collections::HashMap;

/// Given an array of integers `nums` and an integer `target`, return
/// indices of the two numbers such that they add up to `target`.
///
/// You may assume that each input would have exactly one solution,
/// and you may not use the same element twice.
///
/// You can return the answer in any order.
///
/// https://leetcode.com/problems/two-sum/
///
/// ## Constraints:
/// - `2 <= nums.length <= 10^4`
/// - `-10^9 <= nums[i] <= 10^9`
/// - `-10^9 <= target <= 10^9`
/// - Only one valid answer exists.
pub fn two_sum(nums: Vec<i32>, target: i32) -> Vec<i32> {
    let mut buffer = HashMap::new();
    let mut result = vec![];

    for (i, num) in nums.iter().enumerate() {
        let diff = target - num;
        let i = i as i32;

        match buffer.get(&diff) {
            None => {
                buffer.insert(*num, i);
            }
            Some(value) => {
                result = vec![*value, i];
                break;
            }
        }
    }

    result
}

#[cfg(test)]
mod examples {
    use crate::two_sum::two_sum;

    #[test]
    fn example_1() {
        let nums = vec![2, 7, 11, 15];
        let target = 9;

        assert_eq!(vec![0, 1], two_sum(nums, target));
    }

    #[test]
    fn example_2() {
        let nums = vec![3, 3];
        let target = 6;

        assert_eq!(vec![0, 1], two_sum(nums, target));
    }

    #[test]
    fn example_3() {
        let nums = vec![3, 2, 4];
        let target = 6;

        assert_eq!(vec![1, 2], two_sum(nums, target));
    }
}
